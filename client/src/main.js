import Vue from 'vue'
import App from './App'
import router from './router'
import { sync } from 'vuex-router-sync'
import store from '@/store/store'
import '@/assets/bootstrap.min.css'
import 'font-awesome/css/font-awesome.css'
import '@/assets/sb-admin.css'

import Bredcrumb from '@/components/global/bredcrumb'
import Alert from '@/components/global/erroalert'
import Sucalert from '@/components/global/sucalert'

Vue.config.productionTip = false

Vue.component('bredcrumb', Bredcrumb)
Vue.component('alert', Alert)
Vue.component('sucalert', Sucalert)

sync(store, router)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
