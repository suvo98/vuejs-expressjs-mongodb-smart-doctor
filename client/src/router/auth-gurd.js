// import {store} from '../store/store'

export default (to, from, next) => {
  if (to.meta.requiresAuth) {
    const authUser = JSON.parse(window.localStorage.getItem('authUser'))
    if (authUser && authUser.token) {
      next()
    } else {
      next({
        name: 'Login'
      })
    }
  } else {
    next({
      name: 'Login'
    })
  }
}
