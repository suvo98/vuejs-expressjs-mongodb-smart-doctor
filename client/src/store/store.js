import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
Vue.use(Vuex)

export default new Vuex.Store({
  strict: true,
  plugins: [
    createPersistedState()
  ],
  state: {
    navigateCollapse: false,
    isLoggedIn: false,
    user: null
  },
  mutations: {
    setNavToggle (state) {
      state.navigateCollapse = true
    },
    unSetNavToggle (state) {
      state.navigateCollapse = false
    },
    setToken (state, token) {
      state.token = token
      if (token) {
        state.isLoggedIn = true
      } else {
        state.isLoggedIn = false
      }
    },
    setUser (state, user) {
      state.user = user
    },
    clearUser (state) {
      state.isLoggedIn = false
      state.user = null
    }
  },
  actions: {
    setNavToggle ({commit}) {
      commit('setNavToggle')
    },
    unSetNavToggle ({commit}) {
      commit('unSetNavToggle')
    },
    setToken ({commit}, token) {
      commit('setToken', token)
    },
    setUser ({commit}, user) {
      commit('setUser', user)
    },
    clearUser ({commit}) {
      commit('clearUser')
    }
  },
  getters: {
    user (state) {
      return state.user
    }
  }
})
