var passport = require('passport');
var jwt = require('jsonwebtoken');
var User = require('../models/user');
var config = require('../config/database');

const AuthController = require('../controllers/authCtrl');

module.exports = function(app, passport){
    
    app.get('/', function (req, res) {
        res.json('Welcome to my nodejs application');
    })

    app.post('/signup' , function (req, res) {
        var newUser = new User({
           firstName: req.body.firstName,
           lastName: req.body.lastName,
           email: req.body.email, 
           password: req.body.password 
        }); 
        User.createUser(newUser, function (err, user) {
           if (err) {
              res.json({success:false, message:'User is not registered'});
           } else {
            res.json({success:true, message:'User is registered'});  
           }
        })
    });

    app.post('/login', function (req, res) {
        var email = req.body.email;
        var password = req.body.password;
        User.getUserByEmail(email, function (err, user) {
            if (err) throw err;
            if (!user) {
                return  res.json({success:false, message:'User is not found'});
            }
            
            User.comparePassword(password, user.password , function (err, isMatch) {
                if (err) throw err;
                if (isMatch) {
                    
                    // var token = jwt.sign(user, config.secret, {expiresIn:60});
                    var token = jwt.sign({
                        data: user
                      }, config.secret, { expiresIn: 60 * 60 });

                    res.json({
                        success: true,
                        token: 'bearer ' + token,
                        user: {
                            id: user._id,
                            firstName: user.firstName,
                            email: user.email,
                            password: user.password
                        }
                    })
                } else {
                    return res.status(400).send({
                        error: 'Password does not match'
                      })
                }
            }) 
        });
    })

    app.get('/profile', passport.authenticate('jwt', { session: false }),
    function(req, res, next) {
        res.json({user: req.user});
    })
    
    app.get('/logout', function (req, res) {
        res.logout();
        res.redirect('/');
    })

    app.get('/isEmailExist/:email', 
       AuthController.checkEmail
    )
 }