const User = require('../models/user')

module.exports = {
   checkEmail (req, res) {
      try {
         const email = req.params.email
         return User.findOne({email: email}, function (err, user) {
            if (user) {
                res.json({
                    success: false,
                    data: 'Email Not Available'
                })
            } else {
                res.json({
                    success: true,
                    data: 'Email Available'
                })
            }
         })
         
      } catch (error) {
        res.status(500).send({
            error: 'An error was occured when try to fetch songs'
          })
      }
  }
}
